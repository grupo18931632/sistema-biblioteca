package Clases;

import java.util.Date;

public class Autor {
    
    private int idAutor;
    private String nombre;
    private String nacionalidad;
    private Date fechaNacimiento;

    public Autor ()
    {}

    public Autor (int idAutor,String nombre, String nacionalidad, Date fechaNacimiento)
    {
        this.idAutor = idAutor;
        this.nombre = nombre;
        this.nacionalidad = nacionalidad;
        this.fechaNacimiento = fechaNacimiento;
    }

    public void crearAutor(Autor _Autor)
    {
        System.out.println("Nuevo Registro" + _Autor.nombre);
    }

    public void modificarAutor(Autor _Autor)
    {
        System.out.println("Modificar Registro" + _Autor.nombre);
    }

}
